# Wipro Test Excerise README

## Brief Summary
The 2 Rest assured tests are covered as 3 scenarios. I have done 2 separate scenarios for Scenario 4. One in the traditional way using just Rest Assured and 2nd one using POJO Deserialization, AssertJ and little bit of Java 8 features.

## Repo
Repo is situated at the following address:
```
https://bitbucket.org/jonn-en/wpro-test
```

## Running the tests

### Using Test runner
This will only run the tests not annotated with `@ignore`.
To run the test with the test runner, run the below command in the project home
```
mvn clean -Dtest=TestRunner test
```

### Using the Feature file
If you want to run all the tests, irrespective of `@ignore` tag, you can run the Feature file directly from the IDE, like IntelliJ or Eclipse.

## Report
This will be generated in the `target/cucumber-report` folder. The report is basic but will give you idea success/failure in the test run.

## NOTE
For Scenario 4, I have used a POJO because it needs the property of an object to be accessed and checked. Whereas, the 2nd test only need you to count the number of records returned in the response, POJO would have been an overkill there.