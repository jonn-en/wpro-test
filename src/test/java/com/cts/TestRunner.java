package com.cts;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:features",
        glue = "com.cts.stepdef",
        tags = "~@ignore",
        format = {"pretty", "html:target/cucumber-report"})
public class TestRunner {
}
