package com.cts.stepdef;

import com.cts.domain.Comment;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import org.assertj.core.api.SoftAssertions;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

public class RestStepDefinitions {

    private String webServiceURI;
    private Response response;

    @Given("^Web Service is running on the below URL$")
    public void webServiceIsRunningOnTheBelowURL(List<String> url) throws Throwable {
        webServiceURI = url.get(0);
    }

    @When("^a GET call is made to end-point \"([^\"]*)\"$")
    public void aGETCallIsMadeToEndPoint(String endPoint) throws Throwable {
        String uri = webServiceURI.endsWith("/") ? webServiceURI + endPoint : webServiceURI + "/" + endPoint;
        response = given().get(uri);
    }

    @Then("^the Success Response is received$")
    public void theASuccessResponseIsReceived() throws Throwable {
        assertThat(response.statusCode()).isEqualTo(200);
    }

    @And("^the comment has the below \"([^\"]*)\"$")
    public void theCommentHasTheBelowEmail(String email) throws Throwable {
        Comment actualComment = response.as(Comment.class);
        assertThat(actualComment.getEmail()).as("Email address match").isEqualToIgnoringCase(email);

    }

    @And("^the comment has the below attributes$")
    public void theCommentHasTheBelowAttributes(List<Comment> expectedComments) throws Throwable {
        SoftAssertions softAssertions = new SoftAssertions();
        List<Comment> actualComments = Arrays.asList(response.as(Comment[].class));
        assertThat(actualComments.size())
                .as("Comments returned by service is more than expected comments")
                .isGreaterThan(expectedComments.size());

        expectedComments.stream().forEach(expectedComment -> {
            List<Comment> filteredComment = actualComments.stream()
                    .filter(actualComment -> actualComment.getId().equals(expectedComment.getId()))
                    .collect(Collectors.toList());
            softAssertions.assertThat(filteredComment)
                    .as("There is at least 1 comment with the Id: %s", expectedComment.getId())
                    .hasSize(1);
            if (filteredComment.size() > 0) {
                softAssertions.assertThat(filteredComment.get(0))
                        .as("Comments Attributes matches for comment with Id: %s", expectedComment.getId())
                        .isEqualToIgnoringNullFields(expectedComment);
            }
        });
        softAssertions.assertAll();
    }

    @And("^number of records returned is (\\d+)$")
    public void numberOfRecordsReturnedIs(int noOfRecords) throws Throwable {
        assertThat(response.jsonPath().getList("$").size()).as("No of records returned").isEqualTo(noOfRecords);
    }
}
