Feature: Check the Response of the two Rest End points

  Background:
    Given Web Service is running on the below URL
      | https://jsonplaceholder.typicode.com |

  #Scenario 4: Simple way of asserting Email address
  Scenario Outline: Check comments has the right email address
    When a GET call is made to end-point "comments/<id>"
    Then the Success Response is received
    And the comment has the below "<email>"
    Examples:
      | id | email                |
      | 1  | Eliseo@gardner.biz   |
      | 3  | Nikita@garfield1.biz |

  #Scenario 4: a bit complex, just to demonstrate AssertJ, Deserialization & Java 8 usage a bit
  @ignore
  Scenario: Check comments has the right attributes
    When a GET call is made to end-point "comments"
    Then the Success Response is received
    And the comment has the below attributes
      | id | email                |
      | 1  | Eliseo@gardner.biz   |
      | 3  | Nikita@garfield1.biz |

  #Scenario 5
  Scenario: Check the number of users returned is correct
    When a GET call is made to end-point "users"
    Then the Success Response is received
    And number of records returned is 10